import {baseUrl} from './config';

function request(path, method, body) {
  const options = {};
  if (method) options.method = method;
  if (body) options.body = body;

  return fetch(baseUrl + path, options).then(res => res.json());
}

export default request;
