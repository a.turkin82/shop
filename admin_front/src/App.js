import React, {Component} from 'react';
import Products from "./components/Products/Products";
import Categories from "./components/Categories/Categories";
import {BrowserRouter, Route, Switch} from "react-router-dom";
import Home from "./components/Home/Home";
import Page404 from "./components/Page404/Page404";

class App extends Component {
  state = {
    activeTab: 1
  }

  changeTab = activeTab => {
    console.log(activeTab);
    this.setState({activeTab});
  }

  render() {
    return (
      <BrowserRouter>
        <h1>Without router</h1>
        <Switch>
          <Route exact path="/" component={Home}/>
          <Route exact path="/categories" component={Categories}/>
          <Route exact path="/products" component={Products}/>
          <Route path="/" component={Page404}/>
        </Switch>
      </BrowserRouter>
    )


    // return (
    //   <>
    //     <Container>
    //       <h1>Shop KB</h1>
    //       <div className="nav">
    //         <h4 className={this.state.activeTab === 1 ? 'active' : ''}
    //             onClick={() => this.changeTab(1)}
    //         >
    //           Добавить товар
    //         </h4>
    //         <h4 className={this.state.activeTab === 2 ? 'active' : ''}
    //             onClick={() => this.changeTab(2)}
    //         >
    //           Добавить Категорию
    //         </h4>
    //       </div>
    //     </Container>
    //
    //     {this.state.activeTab === 1 ? <Products/> : null}
    //     {this.state.activeTab === 2 ? <Categories/> : null}
    //   </>
    // );
  }
}

export default App;
