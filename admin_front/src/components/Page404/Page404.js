import React, {useState} from 'react';
import Layout from "../Layout/Layout";

const Page404 = props => {

  const [num, setNum] = useState(404);

  const increment = () => {
    setNum(num + 1);
  }

  const decrement = () => {
    setNum(num - 1);
  }

  return (
    <Layout {...props}>
      <div className="page404">
        <h1 onClick={increment}>Error {num}</h1>
        <h3 onClick={decrement}>Page not found</h3>
      </div>
    </Layout>
  );
};

export default Page404;
