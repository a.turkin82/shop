import React, {Component} from 'react';
import Layout from "../Layout/Layout";

class Home extends Component {
  render() {
    return (
      <Layout {...this.props}>
        <h1>Main page</h1>
      </Layout>
    );
  }
}

export default Home;
